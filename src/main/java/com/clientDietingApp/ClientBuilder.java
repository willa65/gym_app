package com.clientDietingApp;

import java.util.ArrayList;

public class ClientBuilder {


    protected String clientName;
    protected int age;
    protected double weightInKg;
    //set via set macros
    protected double protein;
    protected double fat;
    protected double carbs;
    protected int estMaintKcals;
    //derived
    protected double weightInLb;
    protected double activityMultiplier;
    private String goal;

    ClientBuilder(String name, int age, double weightInKg, double activityMultiplier) {
        this.clientName = name;
        this.weightInKg = weightInKg;
        this.activityMultiplier = activityMultiplier;
        //derived fields
        setKgToLb();
        this.estMaintKcals = (int) (weightInLb * activityMultiplier) * 10;
        setMacros();

    }

    ClientBuilder(String name, int age, double weightInKg) {
        this.clientName = name;
        this.weightInKg = weightInKg;
        this.activityMultiplier = 1.6;
        //derived fields
        setKgToLb();
        this.estMaintKcals = (int) (weightInLb * activityMultiplier) * 10;
        setMacros();
    }

    public void setMacros() {
        this.protein = weightInLb * 1;
        this.fat = weightInLb * 0.4;
        this.carbs = (estMaintKcals - ((protein * 4) + (fat * 9))) / 4;
    }

    public void setKgToLb() {
        weightInLb = this.weightInKg *= 2.204;
    }

    public void toStringMacros() {
        System.out.println("Protein = " + protein + "\nFats = " + fat + "\nCarbs = " + carbs + "\nTotal Kcals = " + estMaintKcals + "\n");
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeightInKg() {
        return weightInKg;
    }

    public void setWeightInKg(double weightInKg) {
        this.weightInKg = weightInKg;
    }

    public double getProtein() {
        return protein;
    }

    public double getFat() {
        return fat;
    }

    public double getCarbs() {
        return carbs;
    }

    public int getEstMaintKcals() {
        return estMaintKcals;
    }

    public double getActivityMultiplier() {
        return activityMultiplier;
    }

    public void setActivityMultiplier(double activityMultiplier) {
        this.activityMultiplier = activityMultiplier;
        this.estMaintKcals = (int) (weightInLb * activityMultiplier) * 10;
        setMacros();
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }


}



