package com.clientDietingApp;

public class ClientWeightLoss extends ClientBuilder {

    protected int deficit;
    protected int dietingKcals = estMaintKcals;

    ClientWeightLoss(String name, int age, double weightInKg, double activityMultiplier) {
        super(name, age, weightInKg, activityMultiplier);
        this.estMaintKcals = (int) (weightInLb * activityMultiplier) * 10;
        setMacros();
        setKgToLb();
    }

    ClientWeightLoss(String name, int age, double weightInKg) {
        super(name, age, weightInKg);
        this.estMaintKcals = (int) (weightInLb * activityMultiplier) * 10;
        setMacros();
        setKgToLb();
    }

    public void setDietingMacros(int deficit) {
        this.deficit = deficit;
        this.protein = weightInLb * 1.1;
        this.fat = weightInLb * 0.3;
        this.dietingKcals -= deficit;
        this.carbs = (this.dietingKcals - ((protein * 4) + (fat * 9))) / 4;
    }


    @Override
    public void toStringMacros() {
        System.out.println("Protein = " + protein + "\nFats = " + fat + "\nCarbs = " + carbs + "\nTotal Kcals = " + dietingKcals + "\n");
    }


}
