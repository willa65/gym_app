package com.clientDietingApp;


import java.util.Scanner;

public class GymAppCLI {

    private static final String MAIN_MENU_OPTION_NEW_CLIENT = "Make New Client";
    private static final String MAIN_MENU_OPTION_PRINT_CLIENT_LIST = "Print Client List";
    private static final String MAIN_MENU_OPTION_EXIT = "Exit";
    private static final String[] optionsList = {"Make New Client", "Print Client List", "Exit"};
    private Menu menu;
    private HelperClientMaker helper = new HelperClientMaker();

    public GymAppCLI(Menu menu) {
        this.menu = menu;
    }


    public static void main(String[] args) {

        Menu menu = new Menu(System.in, System.out);

        GymAppCLI cli = new GymAppCLI(menu);

        cli.run();
    }

    public void run() {

        while (true) {
            Menu runUserDisplay = new Menu(System.in, System.out);

            menu.displayOptionsMenu(optionsList);
            String userChoice = (String) menu.getChoiceFromUserInput(optionsList);
            Scanner userInput = new Scanner(System.in);


            if (userChoice.equals(MAIN_MENU_OPTION_NEW_CLIENT)) {
                helper.makeNewClient();
            }

            if (userChoice.equals(MAIN_MENU_OPTION_PRINT_CLIENT_LIST)) {
                helper.displayList();
            }

            if (userChoice.equals(MAIN_MENU_OPTION_EXIT)) {
                menu.exitProgramme();
            }


        }
    }
}
