package com.clientDietingApp;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class HelperClientMaker {


    private ArrayList<ClientBuilder> clientList = new ArrayList<>();
    private Scanner in;
    private PrintWriter out;


    public void makeNewClient() {

        try {
            Scanner userInput = new Scanner(System.in);
            System.out.println("Enter Clients name");
            String name = userInput.nextLine();
            System.out.println("Enter Clients age");
            int age = userInput.nextInt();
            System.out.println("Enter Clients weight in kg");
            double weight = userInput.nextDouble();
            System.out.println("Enter an activity multiplier");
            double activityMultiplier = userInput.nextDouble();
//        ClientBuilder clientToAdd = new ClientBuilder(name, age, weight, activityMultiplier);
            ClientBuilder newClient = new ClientBuilder(name, age, weight, activityMultiplier);
            this.clientList.add(newClient);
        }catch (InputMismatchException e){
            System.err.println("Incorrect input");
        }
    }


    public void displayList() {
        if(!clientList.isEmpty()){
        for (ClientBuilder client : clientList){
            System.out.println(client.getClientName());
            client.toStringMacros();
            }
        }
        else {
            System.out.println("Client list is empty");
        }
    }

    }
