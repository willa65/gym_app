package com.clientDietingApp;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Menu {

    private PrintWriter out;
    private Scanner in;

    public Menu(InputStream input, OutputStream output) {
        this.out = new PrintWriter(output);
        this.in = new Scanner(input);
    }


    public void displayOptionsMenu(Object[] options) {
        out.println();
        for (int i = 0; i <= options.length - 1; i++) {
            int optionNumber = i + 1;
            out.println(optionNumber + ") " + options[i]);
        }
        out.print(System.lineSeparator() + "Select an option >>>>");
        out.flush();
    }

    public Object getChoiceFromUserInput(Object[] options) {
        Object choice = null;
        String userInput = in.nextLine();
        try {
            int selectedOption = Integer.parseInt(userInput);
            if (selectedOption > 0 && selectedOption <= options.length) {
                choice = options[selectedOption - 1];
            }
        } catch (NumberFormatException e) {
            // eat the exception, an error message will be displayed below since choice will be null
        }
        if (choice == null) {
            out.println(System.lineSeparator() + "*** " + userInput + " is not a valid option ***" + System.lineSeparator());
        }
        return choice;
    }

    public void exitProgramme(){
        System.exit(3);
    }





}





